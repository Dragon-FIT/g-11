#ifndef _QuanLyDaThuc_
#define _QuanLyDaThuc_

#include <iostream>
#include <fstream>
#include <string>
#include <iomanip>
using namespace std;


struct DONTHUC
{
	//x2,n2 dùng để lưu giá trị của phép nhân
	int x1,x2;//Giá trị x
	int n1,n2;//Số mũ của x
	float a;//Hệ số của x
	DONTHUC *pNext;
};
typedef struct DONTHUC Node;

struct DATHUC
{
	DONTHUC *pHead, *pTail;
};
typedef struct DATHUC List;



////Các thao tác xử lý node, list
Node *MakeNode(int x1, int SoMu1, int x2, int SoMu2, float HeSo); //Tạo 1 node chứa thông tin là giá trị x, số mũ và hệ số 
void InitList(List &l);//Khởi tạo danh sách rỗng
int IsEmpty(List l); //Kiểm tra danh sách có rỗng không
int Len(List l); //Tính độ dài của danh sách
void AddHead(List &l, int x1, int SoMu1, int x2, int SoMu2, float HeSo); //Thêm 1 node vào đầu danh sách chi phí O(1)
void AddTail(List &l, int x1, int SoMu1, int x2, int SoMu2, float HeSo); //Thêm 1 node vào cuối danh sách chi phí O(n)
void AddPosk(List &l, int x1, int SoMu1, int x2, int SoMu2, float HeSo); //Thêm 1 node vào vị trí k (ở đây chính là số mũ tương ứng) danh sách
void DelHead(List &l); //Xoá 1 node đầu danh sách
void DelTail(List &l); //Xoá 1 node ở cuối danh sách
void DelPosk(List &l, Node *q, Node *p, int key); //Xoá 1 node ở vị trí k của danh sách

////Các thao tác xử lý theo đề bài
void Input(string file_name, List &l); //Nhập dữ liệu cho danh sách (đa thức)
void Output(string file_name, List &l); //Xuất dữ liệu của danh sách (đa thức) ra file
void RutGonDaThuc(List &l); //Rút gọn đa thức
void HoanVi(Node *p, Node *q); //Hoán vị 2 data của 2 node
void ChuanHoaDaThuc(List &l); //Sắp xếp các phần tử đa thức theo giá trị luỹ thừa tăng dần
List operator + (List a, List b); //Cộng 2 đa thức
List operator - (List a, List b); //Trừ 2 đa thức
List operator * (List a, List b); //Nhân 2 đã thức


#endif