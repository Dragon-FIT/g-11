#include "DeclareQuanLyDaThuc.h"


////Các thao tác xử lý node, list
Node *MakeNode(int x1, int SoMu1, int x2, int SoMu2, float HeSo) //Tạo 1 node chứa thông tin là giá trị x, số mũ và hệ số 
{
	Node *p = new Node;
	if (p == NULL)
		return NULL;//Cấp phát không thành công
	//Nếu cấp phát thành công thì gán giá trị cho node
	p->a = HeSo;
	p->x1 = x1;
	p->n1 = SoMu1;
	p->x2 = x2;
	p->n2 = SoMu2;
	p->pNext = NULL;
	return p;
}

void InitList(List &l)//Khởi tạo danh sách rỗng
{
	l.pHead = l.pTail = NULL;
}

int IsEmpty(List l) //Kiểm tra danh sách có rỗng không
{
	return (!l.pHead) ? 1 : 0;
}

int Len(List l) //Tính độ dài của danh sách
{
	int count = 0;
	for (Node *p = l.pHead; p != NULL; p = p->pNext)
		count++;//Đếm tăng 1 dần để đếm số node
	return count;
}

void AddHead(List &l, int x1, int SoMu1, int x2, int SoMu2, float HeSo) //Thêm 1 node vào đầu danh sách chi phí O(1)
{
	Node *p = MakeNode(x1, SoMu1, x2, SoMu2, HeSo);//Tạo 1 node
	if (IsEmpty(l))//Nếu danh sách rỗng thì đầu và cuối danh sách sẽ chính là p
		l.pHead = l.pTail = p;
	else//Nếu danh sách có phần tử thì p là đầu danh sách
	{
		p->pNext = l.pHead;//Gán node tiếp theo cho p
		l.pHead = p;//Gán p thành đầu danh sách
	}
}

void AddTail(List &l, int x1, int SoMu1, int x2, int SoMu2, float HeSo) //Thêm 1 node vào cuối danh sách chi phí O(1)
{
	Node *p = MakeNode(x1, SoMu1, x2, SoMu2, HeSo);//Tạo 1 node 
	if (IsEmpty(l))//Nếu danh sách rỗng thì đầu và cuối danh sách sẽ chính là p
		l.pHead = l.pTail = p;
	else//Nếu danh sách có phần tử thì p là cuối danh sách
	{
		l.pTail->pNext = p;//Gán node tiếp theo cho node cuối
		l.pTail = p;//p trở thành node cuối
	}
}

void AddPosk(List &l, int x1, int SoMu1, int x2, int SoMu2, float HeSo) //Thêm 1 node vào vị trí k phía sau 1 node (ở đây chính là số mũ tương ứng) danh sách
{
	Node *p = MakeNode(x1, SoMu1, x2, SoMu2, HeSo);//Tạo 1 node
	if (IsEmpty(l))//Nếu danh sách rỗng thì đầu và cuối danh sách sẽ chính là p
		l.pHead = l.pTail = p;
	else
	{
		for (Node *q = l.pHead; q != NULL; q = q->pNext)
		{
			if (p->n1 <= q->n1)//Nếu số mũ của p bé hơn bằng q thì p đứng sau q
			{
				p->pNext = q->pNext;//Node tiếp theo của p sẽ là node tiếp theo của q khi chưa thêm p vào danh sách
				q->pNext = p;//Node tiếp theo của q là p
				break;
			}
		}
	}
}

void DelHead(List &l) //Xoá 1 node đầu danh sách
{
	Node *p = l.pHead;//Gán p là đầu danh sách
	if (IsEmpty(l))//Nếu danh sách rỗng thì đầu và cuối danh sách sẽ chính là p
		cout << "Mang rong!!!";
	else
	{
		l.pHead = p->pNext;//Chuyển pHead sang node kế bên p vì p sẽ bị xoá
		delete p;
	}
}

void DelTail(List &l) //Xoá 1 node ở cuối danh sách
{
	if (IsEmpty(l))
		cout << "Mang rong!!!";
	else
	{
		Node *q = NULL;
		Node *p = l.pHead;
		while (p != l.pTail)
		{
			q = p;//Mục đích: Gán q là node trước pTail
			p = p->pNext;
		}
		
		if (!q)//Nếu q == NULL tương ứng danh sách chỉ có 1 node (pHead = pTail)
			l.pHead = l.pTail = NULL;//Sau khi xoá thì 2 con trỏ pHead pTail rỗng
		else //p lúc này đã trở thành pTail và q là node trước pTail
		{
			q->pNext = NULL;
			l.pTail = q;//Gán lại con trỏ pTail
		}
		delete p;
	}
}

void DelPosk(List &l, Node *q, Node *p, int key) //Xoá 1 node ở vị trí k của danh sách (k là ở đây là hệ số để rút gọn danh sách nếu hệ số bằng 0)
{
	if (IsEmpty(l))
		cout << "Mang rong!!!";
	else
	{
		if (p->a == key && p == l.pHead)//Nếu key bằng hệ số ở đầu thì xoá đầu
			DelHead(l);
		else if (p->a == key && p == l.pTail)//Ở cuối thì xoá cuối
			DelTail(l);
		else
		{
			q->pNext = p->pNext;//Gán node kế tiếp của q là node kế của p
			p->pNext = NULL; //Xoá mắc xích của p với node kế tiếp
			delete p;
		}
	}
}



////Các thao tác xử lý theo đề bài
void Input(string file_name, List &l)//Nhập dữ liệu cho danh sách (đa thức)
{
	fstream fin(file_name, ios::in);
	while (!fin.eof())
	{
		float a;
		int x1, n1;
		char c; 
		fin >> a;
		fin >> c; //Đọc kí tự '*'
		fin >> x1;
		fin >> c; //Đọc kí tự '^'
		if (c != '^') // Trường hợp ko phải là '^' (tức là '+' hoặc '-')
		{
			AddTail(l, x1, 1, 0, 0, a);
			if (!fin.eof()) 
				fin.seekg(-1, ios::cur); //Lùi con trỏ vị trí về 1 đơn vị
		}
		else
		{
			fin >> n1;
			AddTail(l, x1, n1, 0, 0, a);
		}
	}
	fin.close();
}

void Output(string file_name, List &l)//Xuất dữ liệu của danh sách (đa thức) ra file
{
	fstream fout(file_name, ios::app); //Ghi dữ liệu vào cuối file 
	for (Node *p = l.pHead; p != NULL; p = p->pNext)
	{
		if (p->a >= 0)
		{
			if (p != l.pHead) 
				fout << "+";
			fout << setprecision(2) << fixed << p->a << "*" << p->x1;
		}
		else
			fout << std::setprecision(2) << fixed << p->a << "*" << p->x1;
		if (p->n1 != 1) fout << "^" << p->n1;
		if (p->n2 != 0 && p->x2 != 0)
		{
			fout << "*" << p->x2;
			if (p->n2 != 1) 
				fout << "^" << p->n2;
		}
	}
	fout << endl << endl;
	fout.close();
}

void RutGonDaThuc(List &l) //Rút gọn đa thức
{
	Node *p = l.pHead;
	Node *q = NULL;
	while (p)
	{
		if (p->a == 0 || p->x1 == 0)
		{
			Node* temp = p->pNext;
			DelPosk(l, q, p, 0);//Xoá node với hệ số là 0
			p = temp;
		}
		else
		{
			q = p;
			p = p->pNext;
		}
	}
}

void HoanVi(Node *p, Node *q) //Hoán vị 2 data của 2 node
{
	//Hoán vị hệ số
	swap(p->a, q->a);

	//Hoán vị x
	swap(p->x1, q->x1);
	swap(p->x2, q->x2);

	//Hoán vị số mũ
	swap(p->n1, q->n1);
	swap(p->n2, q->n2);
}

void ChuanHoaDaThuc(List &l) //Sắp xếp các phần tử đa thức theo giá trị luỹ thừa tăng dần
{
	//Sắp xếp các phần tử với luỹ thừa tăng dần, nếu cùng số mũ thì sắp xép tăng dần theo giá trị x 
	for (Node *p = l.pHead; p != NULL; p = p->pNext)
	{
		for (Node *q = p->pNext; q != NULL; q = q->pNext)
		{
			if (p->n1 > q->n1) 
				HoanVi(p, q);
			if (p->n1 == q->n1 && p->x1 > q->x1)
				HoanVi(p, q);
		}
	}
	//Gộp các phần tử cùng cơ số x1,x2 và số mũ n1,n2
Loop:	
	for (Node *p = l.pHead; p != NULL; p = p->pNext)
	{
		for (Node *q = p->pNext; q != NULL; q = q->pNext)
		{
			if ((p->n1 == q->n1) && (p->x1 == q->x1) && (p->n2 == q->n2) && (p->x2 == q->x2))
			{
				p->a += q->a;
				DelPosk(l, p, q, p->a);
				goto Loop;
			}
		}
	}
}

List operator + (List a, List b) //Cộng 2 đa thức
{
	if (IsEmpty(a))
		return b;
	if (IsEmpty(b))
		return a;
	List res;
	InitList(res);
	Node *p = a.pHead;
	while (p != NULL)			//res = a
	{
		AddTail(res, p->x1, p->n1, 0, 0, p->a);
		p = p->pNext;
	}
	Node* q = b.pHead;
	while (q != NULL)			//Cộng các đơn thức của b vào res
	{
		int flag = 0;
		Node *p = res.pHead;
		while (p != NULL)
		{
			if (p->x1 == q->x1 && p->n1 == q->n1)
			{
				p->a += q->a;
				flag = 1;
			}
			p = p->pNext;
		}
		if (!flag)//Add đơn thức vào đa thức res gồm các node đã cộng từ nhiều node hoặc các node đơn lẻ
			AddTail(res, q->x1, q->n1, 0, 0, q->a);
		q = q->pNext;
	}
	return res;
}

List operator - (List a, List b) //Trừ 2 đa thức
{
	if (IsEmpty(b))
		return a;
	List res;
	InitList(res);
	Node *p = b.pHead;
	while (p != NULL)			//res = - b
	{
		AddTail(res, p->x1, p->n1, 0, 0, -(p->a));
		p = p->pNext;
	}
	if (IsEmpty(a))
		return res;
	return res + a;
}

List operator * (List a, List b) //Nhân 2 đã thức
{
	List res;
	InitList(res);
	if (IsEmpty(a))
		return res;
	if (IsEmpty(b))
		return res;
	Node *p = a.pHead;
	Node *q = b.pHead;
	Node *k = MakeNode(0, 0, 0, 0, 0);

	while (p != NULL)
	{
		while (q != NULL)
		{
			k->a = p->a * q->a;
			if (p->x1 == q->x1)//Nếu cùng cơ số thì nhân hệ số và cộng số mũ giữ nguyên cơ số. VD 2*3^2 x 3*3^3 = (2x3)*3^(2+3)
			{
				k->n1 = p->n1 + q->n1;
				k->x1 = p->x1;
			}
			if (p->n1 == q->n1)//Nếu cùng số mũ thì nhân hệ số và nhân cơ số giữ nguyên số mũ. VD 2*3^2 x 3*4^2 = (2x3)*(3x4)^2
			{
				k->x1 = p->x1 * q->x1;
				k->n1 = p->n1;
			}
			if (p->x1 != q->x1 && p->n1 != q->n1)//Nếu khác cơ số và số mũ thì nhân hệ số lại với nhau và xếp các dữ liệu theo từng loại
			{
				//Sắp xếp x1, x2 sau khi nhân dựa vào n1, n2 lớn hơn đứng sau, nhỏ hơn đứng trước để tiện cho việc rút gọn chuẩn hoá 
				if (p->n1 < q->n1)
				{
					k->x1 = p->x1;
					k->n1 = p->n1;
					k->x2 = q->x1;
					k->n2 = q->n1;
				}
				else
				{
					k->x1 =	q->x1;
					k->n1 = q->n1;
					k->x2 = p->x1;
					k->n2 = p->n1;
				}
			}
			AddTail(res, k->x1, k->n1, k->x2, k->n2, k->a);
			k->x2 = 0; //Xóa x2, n2 phần cũ
			k->n2 = 0; //Xóa x2, n2 phần cũ
			q = q->pNext;
		}
		p = p->pNext;
		q = b.pHead; //Quay lại đầu danh sách
	}
	return res;
}