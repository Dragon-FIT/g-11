#include "DeclareQuanLyDaThuc.h"


int main()
{

	List l1, l2, l3, l4, l5;
	InitList(l1);
	InitList(l2);
	InitList(l3);
	InitList(l4);
	InitList(l5);
	
	//nhập dữ liệu từ F1 và F2
	Input("F1.txt", l1);
	RutGonDaThuc(l1);
	ChuanHoaDaThuc(l1);

	Input("F2.txt", l2);
	RutGonDaThuc(l2);
	ChuanHoaDaThuc(l2);

	// tính toán
	l3 = l1 + l2;
	RutGonDaThuc(l3);
	ChuanHoaDaThuc(l3);

	l4 = l1 - l2;
	RutGonDaThuc(l4);
	ChuanHoaDaThuc(l4);

	l5 = l1 * l2;
	RutGonDaThuc(l5);
	ChuanHoaDaThuc(l5);

	//ghi kết quả ra F-result.txt
	fstream fout("F-result.txt", ios::out);
	fout << "F1\n";
	fout.close();
	Output("F-result.txt", l1);
	
	fout.open("F-result.txt", ios::app);
	fout << "F2\n";
	fout.close();
	Output("F-result.txt", l2);

	fout.open("F-result.txt", ios::app);
	fout << "F3\n";
	fout.close();
	Output("F-result.txt", l3);

	fout.open("F-result.txt", ios::app);
	fout << "F4\n";
	fout.close();
	Output("F-result.txt", l4);

	fout.open("F-result.txt", ios::app);
	fout << "F5\n";
	fout.close();
	Output("F-result.txt", l5);

	system("pause");


	return 0;
	
}